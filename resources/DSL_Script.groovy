folder('Assignment08')
def DSL_Job = multibranchPipelineJob('Assignment08/Assignment 8')
DSL_Job.with {
    description 'Job to perform DSL multibranch pipeline'
        branchSources {
            branchSource {
                source {
                    git{
                        id('01')
                        remote('https://gitlab.com/iforimran/gittest.git')
                    }
                }
                strategy {
                    defaultBranchPropertyStrategy {
                        props {
                            noTriggerBranchProperty()
                        }
                    }
                }
            }
        }
        configure {
            def traits = it / 'sources' / 'data' / 'jenkins.branch.BranchSource' / 'source' / 'traits'
            traits << 'jenkins.plugins.git.traits.BranchDiscoveryTrait' {}
        }
        factory {
            workflowBranchProjectFactory {
                scriptPath('Jenkinsfile')
            }
        }
        orphanedItemStrategy {
            discardOldItems {
                numToKeep(20)
            }
        }
    }
