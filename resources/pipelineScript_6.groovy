@Library('myLibrary01') _
pipeline {
    agent any
    stages {
        stage('GitClone'){
           steps{
                git 'https://github.com/opstree/spring3hibernate.git'
            }
        }
        stage('code stability') {
            steps {
                script{
                    codeStability.codeStability()   
                }
            }
        }
        
        stage('Going parallel') {
            parallel {
                stage('Code Quality') {
                    steps {
                        script{
                            codeQuality.codeQuality()
                        }
                    }
                }
                stage ('Code coverage') {
                    steps{
                        script{
                            codeCoverage.codeCoverage()
                        }
                    }
                }
            }
        
        }
        stage('Coverage Report'){
            steps{
                sh 'mvn pmd:pmd'
            }
        }

        stage('workspace cleanup'){
            steps{
                sh 'mvn clean'
            }
        }
    }
}
