@Library('myLibrary01') _
pipeline {
    agent any
    stages {
        stage('GitClone'){
           steps{
                git 'https://github.com/opstree/spring3hibernate.git'
            }
        }
        stage('code stability') {
            steps {
                script{
                    windowsLib.codeStability()   
                }
            }
        }
        
        stage('') {
            parallel {
                stage('Code Quality') {
                    steps {
                        script{
                            windowsLib.codeQuality()
                        }
                    }
                }
                stage ('Code coverage') {
                    steps{
                        script{
                            windowsLib.codeCoverage()
                        }
                    }
                }
            }
        }
        stage('Coverage Report'){
            steps{
                script{
                    windowsLib.coverageReport()
                }
            }
        }

        stage('workspace cleanup'){
            steps{
                script{
                    windowsLib.cleanup()
                } 
            }
        }
    }
}
