def codeCoverage(){
	bat 'mvn cobertura:cobertura'
}

def codeStability(){
	bat 'mvn compile'
}

def codeQuality(){
	bat 'mvn checkstyle:checkstyle'
}

def coverageReport(){
    bat 'mvn pmd:pmd'
}

def cleanup(){
	bat 'mvn clean'
}

